export interface Author {
    id?: number;
    name?: string;
    lastname?: string;
    birthdate?: Date | string;
    books?: Book[];
};

export interface Genre {
    id?: number;
    label?: string;
    books?: Book[];
}

export interface User {
    id?: number;
    firstname?: string;
    lastname?: string;
    email?: string;
    bookshop?: string;
    password?: string;
    role?: string
}

export interface Order {
    id?: number;
    receptionDate?: Date | string;
    bookQuantity?: number;
    total?: number;
    comment?: string;
    books?: Book[];
    user?: User;
}

export interface Publisher {
    id?: number;
    label?: string;
}

export interface Book {
    id?: number;
    title?: string;
    isbn?: string;
    publicationDate?: Date | string;
    comment?: string;
    stock?: number;
    isAvailable?: boolean;
    price?: number;
    genre?: Genre;
    author?: Author;
    publisher?: Publisher;
    order?: Order[];
}

export interface Sale {
    id?: number;
    saleDate?: Date | string;
    bookQuantity?: number;
    books: Book[];
    user?: User;
}

export interface Bookshop {
    id?: number;
    name?: string;
    address?: string;
    zipcode?: string;
    city?: string;
    booksellers?: User[];
}