import LoginPageView from '@/views/LoginPageView'
import React from 'react'

function login() {
    return (
        <main>
            <LoginPageView />
        </main>
    )
}

export default login
