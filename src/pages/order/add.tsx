import AddOrderPageView from '@/views/AddOrderPageView'
import React from 'react'

function addOrder() {
  return (
    <>
      <main
        className="md:flex hidden ml-[25%] flex-col w-screen md:w-3/4 pt-10"
      >
        <AddOrderPageView />
      </main>
      <main
        className="flex md:hidden flex-col w-screen md:w-3/4 pt-10"
      >
        <AddOrderPageView />
      </main>
    </>
  )
}

export default addOrder
