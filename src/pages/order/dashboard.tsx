import { AuthContext } from '@/auth/auth-context';
import { Order } from '@/entities';
import { fetchAllOrders } from '@/order-service';
import OrderDashboardPageView from '@/views/OrderDashboardPageView';
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react'

function dashboard() {
    const router = useRouter();
    const { token } = useContext(AuthContext);
    const [orders, setOrders] = useState<Order[]>([]);

    useEffect(() => {
        fetchAllOrders()
            .then(data => setOrders(data))
            .catch(error => {
                if (error.response === 401 || token === null || token === undefined) {
                    router.replace('/login');
                }
            });
    }, []);

    return (
        <>
            <main
                className="md:flex hidden ml-[25%] flex-col w-screen md:w-3/4 pt-10"
            >
                <OrderDashboardPageView orders={orders} />
            </main>
            <main
                className="flex md:hidden flex-col w-screen md:w-3/4 pt-10"
            >
                <OrderDashboardPageView orders={orders} />
            </main>
        </>
    )
}

export default dashboard
