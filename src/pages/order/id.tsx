import OneOrderPageView from '@/views/OneOrderPageView'
import React from 'react'

function OneOrderPage() {
    return (
        <>
            <main
                className="md:flex hidden ml-[25%] flex-col w-screen md:w-3/4 pt-10"
            >
                <OneOrderPageView />
            </main>
            <main
                className="flex md:hidden flex-col w-screen md:w-3/4 pt-10"
            >
                <OneOrderPageView />
            </main>
        </>
    )
}

export default OneOrderPage
