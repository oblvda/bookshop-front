import AddSalePageView from '@/views/AddSalePageView'
import React from 'react'

function addSale() {
  return (
    <>
      <main
        className="md:flex hidden ml-[25%] flex-col w-screen md:w-3/4 pt-10"
      >
        <AddSalePageView />
      </main>
      <main
        className="flex md:hidden flex-col w-screen md:w-3/4 pt-10"
      >
        <AddSalePageView />
      </main>
    </>
  )
}

export default addSale