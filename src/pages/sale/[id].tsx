import { AuthContext } from '@/auth/auth-context';
import { Sale } from '@/entities';
import { fetchOneSale } from '@/sale-service';
import OneSalePageView from '@/views/OneSalePageView';
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react';

function OneBook() {
  const router = useRouter();
  const { id } = router.query;
  const { token } = useContext(AuthContext);
  const [sale, setSale] = useState<Sale>();

  useEffect(() => {
    fetchOneSale(String(id))
      .then(data => setSale(data))
      .catch(error => {
        if (error.response == 404) {
          router.push('/404');
        } else if (error.response === 401 || token === null || token === undefined) {
          router.replace('/login');
        }
      });
  }, [id]);

  return (
    <>
      <main
        className="md:flex hidden ml-[25%] flex-col w-screen md:w-3/4 pt-10"
      >
        <OneSalePageView sale={sale} />
      </main>
      <main
        className="flex md:hidden flex-col w-screen md:w-3/4 pt-10"
      >
        <OneSalePageView sale={sale} />
      </main>
    </>
  )
}

export default OneBook
