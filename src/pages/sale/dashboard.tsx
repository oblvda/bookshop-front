import { AuthContext } from '@/auth/auth-context';
import { Sale } from '@/entities';
import { fetchAllSales } from '@/sale-service';
import SaleDashboardPageView from '@/views/SaleDashboardPageView'
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react'

function dashboard() {
    const router = useRouter();
    const { token } = useContext(AuthContext);
    const [sales, setSales] = useState<Sale[]>([]);

    useEffect(() => {
        fetchAllSales()
            .then(data => setSales(data))
            .catch(error => {
                if (error.response === 401 || token === null || token === undefined) {
                    router.replace('/login');
                }
            });
    }, []);

    return (
        <>
            <main
                className="md:flex hidden ml-[25%] flex-col w-screen md:w-3/4 pt-10"
            >
                <SaleDashboardPageView sales={sales} />
            </main>
            <main
                className="flex md:hidden flex-col w-screen md:w-3/4 pt-10"
            >
                <SaleDashboardPageView sales={sales} />
            </main>
        </>
    )
}

export default dashboard
