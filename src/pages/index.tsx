import { AuthContext } from "@/auth/auth-context";
import { fetchAllBooks } from "@/book-service";
import { Book } from "@/entities";
import DashboardPageView from "@/views/DashboardPageView";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";

export default function Home() {
  const router = useRouter();
  const { token } = useContext(AuthContext)
  const [books, setBooks] = useState<Book[]>([]);

  useEffect(() => {
    fetchAllBooks()
      .then(data => setBooks(data))
      .catch(error => {
        if (error.response === 401 || token === null || token === undefined) {
          router.replace('/login');
        }
      });
  }, []);

  return (
    <>
      <main
        className="md:flex hidden ml-[25%] flex-col w-screen md:w-3/4 pt-10"
      >
        <DashboardPageView books={books} />
      </main>
      <main
        className="flex md:hidden flex-col w-screen md:w-3/4 pt-10"
      >
        <DashboardPageView books={books} />
      </main>
    </>
  )
}

