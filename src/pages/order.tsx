import OrderDashboardPageView from '@/views/OrderDashboardPageView'
import React from 'react'

function order() {
    return (
        <>
            <main
                className="md:flex hidden ml-[25%] flex-col w-screen md:w-3/4 pt-10"
            >
                <OrderDashboardPageView />
            </main>
            <main
                className="flex md:hidden flex-col w-screen md:w-3/4 pt-10"
            >
                <OrderDashboardPageView />
            </main>
        </>
    )
}

export default order
