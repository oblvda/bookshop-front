import { AuthContext } from '@/auth/auth-context';
import { fetchOneBook } from '@/book-service';
import { Book } from '@/entities';
import OneBookPageView from '@/views/OneBookPageView'
import { useRouter } from 'next/router'
import React, { useContext, useEffect, useState } from 'react'

function OneBook() {
  const router = useRouter();
  const { id } = router.query;
  const { token } = useContext(AuthContext)
  const [book, setBook] = useState<Book>();

  useEffect(() => {
    fetchOneBook(String(id))
      .then(data => setBook(data))
      .catch(error => {
        if (error.response === 404) {
          router.push('/404');
        } else if (error.response === 401 || token === null || token === undefined) {
          router.replace('/login');
        }
      });
  }, [id]);

  return (
    <>
      <main
        className="md:flex hidden ml-[25%] flex-col md:w-3/4 pt-10"
      >
        <OneBookPageView book={book} />
      </main>
      <main
        className="flex md:hidden flex-col md:w-3/4 pt-10"
      >
        <OneBookPageView book={book} />
      </main>
    </>
  )
}

export default OneBook
