import AddBookPageView from '@/views/AddBookPageView'
import React from 'react'

function addBook() {
  return (
    <>
      <main
        className="md:flex hidden ml-[25%] flex-col w-screen md:w-3/4 pt-10"
      >
        <AddBookPageView />
      </main>
      <main
        className="flex md:hidden flex-col w-screen md:w-3/4 pt-10"
      >
        <AddBookPageView />
      </main>
    </>
  )
}

export default addBook