import axios from "axios";
import { Genre } from "./entities";

export async function fetchAllGenres() {
    const response = await axios.get<Genre[]>(`http://localhost:8000/api/genre`);
    return response.data;
}