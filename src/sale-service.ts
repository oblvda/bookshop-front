import axios from "axios";
import { Book, Sale } from "./entities";

export async function fetchAllSales() {
    const response = await axios.get<Sale[]>(`http://localhost:8000/api/sale`);
    return response.data;
}

export async function fetchOneSale(id: string) {
    const response = await axios.get<Sale>(`http://localhost:8000/api/sale/${id}`);
    return response.data;
}

export async function postSale(sale: Sale) {
    const response = await axios.post<Sale>(`http://localhost:8000/api/sale`, sale);
    return response.data;
}

export async function sellBook(sale: Sale) {
    const response = await axios.put<Sale>(`http://localhost:8000/api/sale/${sale.idBook}/${sale.bookQuantity}/${sale.saleDate}`, sale);
    return response.data;
}