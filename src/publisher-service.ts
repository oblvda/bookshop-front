import axios from "axios";
import { Publisher } from "./entities";

export async function fetchAllPublishers() {
    const response = await axios.get<Publisher[]>(`http://localhost:8000/api/publisher`);
    return response.data;
}