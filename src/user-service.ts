import axios from "axios";
import { User } from "./entities";

export async function fetchAllUsers() {
    const response = await axios.get<User[]>(`http://localhost:8000/api/user`);
    return response.data;
}

export async function fetchCurrentUser() {
    const response = await axios.get<User>(`http://localhost:8000/api/user/current`);
    return response.data;
}