import axios from "axios";
import { Author } from "./entities";

export async function fetchAllAuthors() {
    const response = await axios.get<Author[]>(`http://localhost:8000/api/author`);
    return response.data;
}