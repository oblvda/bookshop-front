import axios from "axios";
import { Order } from "./entities";

export async function fetchAllOrders() {
    const response = await axios.get<Order[]>(`http://localhost:8000/api/order`);
    return response.data;
}

export async function fetchOneOrder(id: string) {
    const response = await axios.get<Order>(`http://localhost:8000/api/sale/${id}`);
    return response.data;
}

export async function postOrder(order: Order) {
    const response = await axios.post<Order>(`http://localhost:8000/api/order`, order);
    return response.data;
}

export async function getOrder(order: Order) {
    const response = await axios.put<Order>(`http://localhost:8000/api/sale/${order.idBook}/${order.bookQuantity}/${order.receptionDate}`, order);
    return response.data;
}