import SaleDashboardTable from '@/components/Sale/SaleDashboardTable';
import { Sale } from '@/entities'
import React from 'react'

interface Props {
    sales?: Sale[];
}

function SaleDashboardPageView({ sales }: Props) {
    return (
        <>
            <div
            >
                <h2
                    className='flex flex-row text-2xl justify-center text-center mb-10'
                >
                    {`Inventaire`}
                </h2>
                <div
                    className='flex flex-row grid grid-flow-col justify-stretch overflow-auto'
                >
                    <SaleDashboardTable sales={sales} />
                </div>
            </div>
        </>
    )
}

export default SaleDashboardPageView
