import { postBook } from '@/book-service'
import BookForm from '@/components/BookForm/BookForm'
import { Book } from '@/entities'
import { useRouter } from 'next/router'
import React from 'react'

function AddBookPageView() {
    const router = useRouter();

    async function addBook(book: Book) {
        const added = await postBook(book);
        router.push('/book/' + added.id);
    }

    return (
        <>
            <div
                className='flex justify-center'
            >
                <h2
                    className='flex text-2xl justify-center text-center mb-5'
                >
                    {`Ajouter un livre`}
                </h2>
            </div>
            <div
                className='items-center flex-wrap mb-10 flex flex-row'>
                <div
                    className='justify-center items-center flex-wrap flex flex-row'
                >
                    <BookForm onSubmit={addBook} />
                </div>
            </div>
        </>
    )
}

export default AddBookPageView
