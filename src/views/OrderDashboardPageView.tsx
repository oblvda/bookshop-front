import OrderTable from '@/components/Order/OrderTable'
import { Order } from '@/entities';
import React from 'react'

interface Props {
    orders?: Order[];
}

function OrderDashboardPageView({ orders }: Props) {
    return (
        <>
            <div
            >
                <h2
                    className='flex flex-row text-2xl justify-center text-center mb-10'
                >
                    {`Gestion des commandes`}
                </h2>
                <div
                    className='flex flex-row grid grid-flow-col justify-stretch overflow-auto'
                >
                    <OrderTable orders={orders} />
                </div>
            </div>
        </>
    )
}

export default OrderDashboardPageView
