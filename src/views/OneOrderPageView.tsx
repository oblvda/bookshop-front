import EditButton from '@/components/Order/EditButton'
import OrderBookCount from '@/components/Order/OrderBookCount'
import OrderBookseller from '@/components/Order/OrderBookseller'
import OrderComment from '@/components/Order/OrderComment'
import OrderDate from '@/components/Order/OrderDate'
import OrderNumber from '@/components/Order/OrderNumber'
import OrderReceptionDate from '@/components/Order/OrderReceptionDate'
import OrderTotal from '@/components/Order/OrderTotal'
import React from 'react'

function OneOrderPageView() {
    return (
        <>
            <div
                className='flex justify-center'
            >
                <h2
                    className='flex text-2xl justify-center text-center mb-10'
                >
                    {`Commande #241`}
                </h2>
            </div>
            <div
                className='flex flex-row flex-wrap justify-center'
            >
                <div
                    className='items-center justify-center flex-wrap mb-10 flex flex-row'>
                    <div
                        className='justify-center items-center flex-wrap flex flex-row'
                    >
                        <OrderNumber />
                        <OrderDate />
                        <OrderBookCount />
                        <OrderBookseller />
                        <OrderReceptionDate />
                        <OrderTotal />
                        <OrderComment />
                    </div>
                </div>
            </div>
            <div
                className='justify-end flex flex-row'
            >
                <EditButton />
            </div>
        </>
    )
}

export default OneOrderPageView
