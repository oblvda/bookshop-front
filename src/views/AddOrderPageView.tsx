import React from 'react'
import OrderNumber from '@/components/Order/OrderNumber'
import OrderDate from '@/components/Order/OrderDate'
import OrderBookCount from '@/components/Order/OrderBookCount'
import OrderBookseller from '@/components/Order/OrderBookseller'
import OrderReceptionDate from '@/components/Order/OrderReceptionDate'
import OrderTotal from '@/components/Order/OrderTotal'
import OrderComment from '@/components/Order/OrderComment'
import ValidateButton from '@/components/Order/ValidateButton'
import AddBookToOrderTable from '@/components/Order/AddBookToOrderTable'

function AddOrderPageView() {
  return (
    <>
      <div
        className='flex justify-center'
      >
        <h2
          className='flex text-2xl justify-center text-center mb-5'
        >
          {`Commande #241`}
        </h2>
      </div>
      <div
        className='items-center flex-wrap mb-10 flex flex-row'>
        <div
          className='justify-center items-center flex-wrap flex flex-row'
        >
          <OrderNumber />
          <OrderDate />
          <OrderBookCount />
          <OrderBookseller />
          <OrderReceptionDate />
          <OrderTotal />
          <OrderComment />
        </div>
      </div >
      <div
        className='flex flex-row grid grid-flow-col justify-stretch mb-10'
      >
        <AddBookToOrderTable />
      </div>
      <div
        className='justify-end flex flex-row'
      >
        <ValidateButton />
      </div>
    </>
  )
}

export default AddOrderPageView
