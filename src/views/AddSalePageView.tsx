import SaleForm from '@/components/SaleForm/SaleForm';
import { Sale } from '@/entities';
import { sellBook } from '@/sale-service';
import { useRouter } from 'next/router';
import React from 'react'

function AddSalePageView() {
    const router = useRouter();

    async function sell(sale: Sale) {
        await sellBook(sale);
        router.push("/sale/dashboard");
    }

    return (
        <>
            <div
                className='flex justify-center'
            >
                <h2
                    className='flex text-2xl justify-center text-center mb-5'
                >
                    {`Vendre un livre`}
                </h2>
            </div>
            <div
                className='items-center justify-center flex-wrap mb-10 flex flex-row'>
                <div
                    className='justify-center items-center flex-wrap flex flex-row'
                >
                    <SaleForm onSubmit={sell} />
                </div>
            </div>
        </>
    )
}

export default AddSalePageView
