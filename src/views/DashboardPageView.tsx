import DashboardTable from '@/components/Dashboard/DashboardTable'
import { Book } from '@/entities'
import React from 'react'

interface Props {
    books: Book[]
}

function DashboardPageView({ books }: Props) {
    return (
        <>
            <div
            >
                <h2
                    className='flex flex-row text-2xl justify-center text-center mb-10'
                >
                    {`Inventaire`}
                </h2>
                <div
                    className='flex flex-row grid grid-flow-col justify-stretch overflow-auto'
                >
                    <DashboardTable books={books} />
                </div>
            </div>
        </>
    )
}

export default DashboardPageView
