import BookAuthor from '@/components/Book/BookAuthor'
import BookComment from '@/components/Book/BookComment'
import BookDate from '@/components/Book/BookDate'
import BookGenre from '@/components/Book/BookGenre'
import BookISBN from '@/components/Book/BookISBN'
import BookPrice from '@/components/Book/BookPrice'
import BookPublisher from '@/components/Book/BookPublisher'
import BookStock from '@/components/Book/BookStock'
import BookTitle from '@/components/Book/BookTitle'
import { Book } from '@/entities'
import React from 'react'

interface Props {
    book?: Book
}

function OneBookPageView({ book }: Props) {
    return (
        <>
            <div
                className='flex justify-center'
            >
                <h2
                    className='flex text-2xl justify-center text-center mb-10'
                >
                    {`Informations du livre`}
                </h2>
            </div>
            <div
                className='flex flex-row flex-wrap justify-center'
            >
                <div
                    className='items-center justify-center flex-wrap mb-20 flex flex-row'>
                    <div
                        className='justify-center items-center flex-wrap flex flex-row'
                    >
                        <BookTitle book={book} />
                        <BookDate book={book} />
                        <BookStock book={book} />
                        <BookGenre book={book} />
                        <BookAuthor book={book} />
                        <BookPublisher book={book} />
                        <BookISBN book={book} />
                        <BookPrice book={book} />
                        <BookComment book={book} />
                    </div>
                </div>
            </div>
        </>
    )
}

export default OneBookPageView
