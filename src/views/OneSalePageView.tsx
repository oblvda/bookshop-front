import SaleBook from '@/components/Sale/SaleBook';
import SaleBookQuantity from '@/components/Sale/SaleBookQuantity';
import SaleDate from '@/components/Sale/SaleDate';
import SaleUser from '@/components/Sale/SaleUser';
import { Sale } from '@/entities'
import React from 'react'

interface Props {
    sale?: Sale;
}

function OneSalePageView({ sale }: Props) {
    return (
        <>
            <div
                className='flex justify-center'
            >
                <h2
                    className='flex text-2xl justify-center text-center mb-10'
                >
                    {`Informations de la vente`}
                </h2>
            </div>
            <div
                className='flex flex-row flex-wrap justify-center'
            >
                <div
                    className='items-center justify-center flex-wrap mb-20 flex flex-row'>
                    <div
                        className='justify-center items-center flex-wrap flex flex-row'
                    >
                        <SaleBook sale={sale} />
                        <SaleDate sale={sale} />
                        <SaleBookQuantity sale={sale} />
                        <SaleUser sale={sale} />
                    </div>
                </div>
            </div>
        </>
    )
}

export default OneSalePageView
