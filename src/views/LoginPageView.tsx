import InputForm from '@/components/Login/LoginForm'
import React from 'react'

function LoginPageView() {
    return (
        <>
            <div
                className='md:flex w-screen h-screen justify-center'
            >
                <div
                    className='md:hidden h-1/4 justify-center items-center flex flex-col bg-[#3100A2]'
                >
                    <img
                        src='/assets/open-book.png'
                        className='object-contain w-1/2 h-1/2'
                    >
                    </img>
                </div>
                <div
                    className='md:flex hidden justify-center items-center w-1/2 bg-[#3100A2]'
                >
                    <img
                        src='/assets/open-book.png'
                        className='object-contain w-1/2 h-1/2'
                    >
                    </img>
                </div>
                <div
                    className='md:w-1/2 flex h-3/4 flex-col items-center justify-center text-center'
                >
                    <h2
                        className='text-2xl text-center my-10'
                    >
                        {`Connexion`}
                    </h2>
                    <div>
                        <InputForm />
                    </div>
                </div>
            </div>
        </>
    )
}

export default LoginPageView
