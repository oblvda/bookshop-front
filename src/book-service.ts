import axios from "axios";
import { Book } from "./entities";

export async function fetchAllBooks() {
    const response = await axios.get<Book[]>(`http://localhost:8000/api/book`);
    return response.data;
}

export async function fetchOneBook(id: string) {
    const response = await axios.get<Book>(`http://localhost:8000/api/book/${id}`);
    return response.data;
}

export async function postBook(book: Book) {
    const response = await axios.post<Book>(`http://localhost:8000/api/book`, book);
    return response.data;
}
