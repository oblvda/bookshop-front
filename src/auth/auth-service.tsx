import { User } from "@/entities";
import axios from "axios";

export async function postLogin(email?: string, password?: string) {
    const response = await axios.post<{ token: string }>('http://localhost:8000/api/login', { email, password });
    return response.data;
}

export async function fetchUser() {
    const repsonse = await axios.get<User>('http://localhost:8000/api/account');
    return repsonse.data;
}