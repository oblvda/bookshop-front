import Link from 'next/link';
import React, { useEffect, useRef, useState } from 'react'
import LogoutButton from './Nav/LogoutButton';

function HamburgerNav() {
    const [isOpen, setIsOpen] = useState(false);
    const menuRef = useRef<HTMLDivElement>(null);

    const toggleMenu = () => {
        setIsOpen(!isOpen);
    };

    const closeMenu = (event: Event) => {
        if (
            isOpen &&
            menuRef.current &&
            !menuRef.current.contains(event.target as Node)
        ) {
            setIsOpen(false);
        }
    };

    useEffect(() => {
        document.addEventListener("click", closeMenu);

        return () => {
            document.removeEventListener("click", closeMenu);
        };
    }, [isOpen]);

    return (
        <>
            <div className='md:hidden flex bg-[#3100A2] h-20'>
                <div
                    className='flex items-center'
                >
                    <img
                        className='flex w-14 h-14 m-5'
                        src='/assets/open-book.png'
                    >
                    </img>
                </div>
                <div
                    className="relative flex w-screen md:hidden justify-end"
                    ref={menuRef}
                >
                    <button
                        className="inline-flex items-center justify-center p-2 rounded-md"
                        aria-expanded={isOpen ? "true" : "false"}
                        onClick={toggleMenu}
                    >
                        <svg
                            className={`${isOpen ? "hidden" : "block"} h-6 w-6 m-5 text-white`}
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            aria-hidden="true"
                        >
                            <path
                                strokeWidth={2}
                                d="M4 6h16M4 12h16m-7 6h7"
                            />
                        </svg>
                    </button>
                    <div
                        className={`${isOpen ? "block" : "hidden"
                            } absolute right-0 mt-2 w-56 origin-top-right`}
                    >
                        <div
                            className='flex grid grid-flow-col justify-stretch items-center text-center h-20 w-52 bg-[#3100A2] text-white border-solid border border-white hover:text-[#3100A2] hover:bg-white'
                        >
                            <Link
                                href={'/'}
                                className=''
                            >
                                {`Inventaire`}
                            </Link>
                        </div>
                        <div
                            className='flex grid grid-flow-col justify-stretch items-center text-center h-20 w-52 bg-[#3100A2] text-white border-solid border border-white hover:text-[#3100A2] hover:bg-white'
                        >
                            <Link
                                href={'/sale/dashboard'}
                                className=''
                            >
                                {`Ventes`}
                            </Link>
                        </div>
                        <div
                            className='flex grid grid-flow-col justify-stretch items-center text-center h-20 w-52 bg-[#3100A2] text-white border-solid border border-white hover:text-[#3100A2] hover:bg-white'
                        >
                            <Link
                                href={'/order/dashboard'}
                                className=''
                            >
                                {`Commandes`}
                            </Link>
                        </div>
                        <div
                            className='flex grid grid-flow-col justify-stretch items-center text-center h-20 w-52 bg-[#3100A2] text-white border-solid border border-white hover:text-[#3100A2] hover:bg-white'
                        >
                            <Link
                                href={'/'}
                                className=''
                            >
                                {`Profil`}
                            </Link>
                        </div>

                        <LogoutButton />
                    </div>
                </div>
            </div>
        </>
    );
}

export default HamburgerNav
