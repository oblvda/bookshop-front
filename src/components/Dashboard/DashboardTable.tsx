import React, { useEffect } from 'react'
import PlusButton from './PlusButton'
import Link from 'next/link'
import { Book } from '@/entities'
import dayjs from 'dayjs'

interface Props {
  books: Book[]
}

function DashboardTable({ books }: Props) {
  return (
    <>
      <table className="border-separate border-spacing-0 table-fixed text-center justify-center">
        <thead className='bg-[#EEEEEE]'>
          <tr>
            <th
              className='p-2 h-10 w-60'
            >
              Titre du livre
            </th>
            <th
              className='p-2 h-10 w-60'
            >
              Auteur
            </th>
            <th
              className='p-2 h-10 w-60'
            >
              Date de publication
            </th>
            <th
              className='p-2 h-10 w-60'
            >
              Maison d’édition
            </th>
            <th
              className='p-2 h-10 w-60'
            >
              Stock
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <PlusButton />
            </td>
          </tr>
          {books?.map(book =>
            <tr key={book.id}>
              <td
                className='p-2 h-10 w-60'
              >
                <Link
                  href={'/book/' + book.id}
                  className='underline'
                >
                  {book.title}
                </Link>

              </td>
              <td
                className='p-2 h-10 w-60'
              >
                {book.author?.name} {book.author?.lastname}
              </td>
              <td
                className='p-2 h-10 w-60'
              >
                {dayjs(book.publicationDate).format("YYYY")}
              </td>
              <td
                className='p-2 h-10 w-60'
              >
                {book.publisher?.label}
              </td>
              <td
                className='p-2 h-10 w-60'
              >
                {book.stock}
              </td>
            </tr>
          )}
        </tbody>
      </table >
    </>
  )
}

export default DashboardTable
