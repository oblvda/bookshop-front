import { fetchAllBooks } from '@/book-service';
import { Book, Order, User } from '@/entities';
import { fetchCurrentUser } from '@/user-service';
import { useRouter } from 'next/router';
import React, { FormEvent, useEffect, useState } from 'react'
import ValidateButton from './ValidateButton';

interface Props {
    onSubmit: (order: Order) => void;
}

function OrderForm({ onSubmit }: Props) {
    const router = useRouter();
    const [currentUser, setCurrentUser] = useState<User>();
    const [books, setBooks] = useState<Book[]>([]);
    const [errors, setErrors] = useState('');
    const [order, setOrder] = useState<Order>({
        receptionDate: '',
        bookQuantity: 0,
        total: 0,
        comment: '',
        user: { id: 0 },
    });

    useEffect(() => {
        fetchCurrentUser()
            .then(data => {
                setCurrentUser(data);
            })
            .catch(error => {
                if (error.response == 404) {
                    router.push('/404');
                }
            });
        fetchAllBooks()
            .then(data => setBooks(data))
            .catch(error => {
                if (error.response == 404) {
                    router.push('/404');
                }
            });
    }, []);

    function handleChange(event: any) {
        const value = event.target.type === 'number' ? parseInt(event.target.value) : event.target.value;
        setOrder({
            ...order,
            user: currentUser,
            [event.target.name]: value
        });
    }

    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        setErrors("");
        try {
            onSubmit(order);
        } catch (error: any) {
            if (error.response.status === 400) {
                setErrors("Merci de renseigner des informations valides");
                return;
            }
        }
    }

    return (
        <>
            {errors &&
                <p
                    className='text-red-500'
                >
                    {errors}
                </p>
            }
            <form
                className='justify-center items-center flex-wrap flex flex-row'
                onSubmit={handleSubmit}
            >
                <div
                    className='flex flex-col'
                >
                    <div
                        className='flex flex-row flex-wrap justify-center'
                    >
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='flex bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`Date de la vente`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody
                                className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center items-center'
                            >
                                <tr>
                                    <td>
                                        <input
                                            required
                                            type="date"
                                            name="receptionDate"
                                            value={String(order?.receptionDate)}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`Quantité`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td
                                        className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                                    >
                                        <input
                                            required
                                            type="number"
                                            name="bookQuantity"
                                            value={Number(order?.bookQuantity)}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`Livre vendu`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td
                                        className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                                    >
                                        <div>
                                            {`À modifier`}
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'>
                                        {`Vendu par`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr
                                    className='p-2 h-full w-60'
                                >
                                    <td
                                        className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center items-center'
                                    >
                                        <div
                                            className='p-2 w-full w-60'
                                        >
                                            {currentUser?.firstname} {currentUser?.lastname}
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5 md:w-full flex-1'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`Commentaire`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td
                                        className='p-2 h-64 flex border border-[#CECECE] bg-[#EEEEEE]'
                                    >
                                        <textarea
                                            className="bg-[#EEEEEE] w-full h-full"
                                            name="comment"
                                            value={order?.comment}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div
                        className='justify-end flex flex-row'
                    >
                        <ValidateButton />
                    </div>
                </div>
            </form>
        </>
    )
}

export default OrderForm
