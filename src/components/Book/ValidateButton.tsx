import React from 'react'

function ValidateButton() {
    return (
        <>
            <button
                className='grid grid-flow-col justify-stretch items-center h-20 w-80 bg-[#3100A2] text-white hover:text-[#3100A2] hover:bg-white mx-5 mb-5'
                type='submit'
            >
                {`Valider`}
            </button>
        </>
    )
}

export default ValidateButton