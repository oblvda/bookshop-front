import { Book } from '@/entities';
import React from 'react'

interface Props {
    book?: Book;
}

function BookStock({ book }: Props) {
    return (
        <>
            <table
                className='table-fixed text-center justify-center m-5'
            >
                <thead
                    className='bg-[#EEEEEE] border border-[#EEEEEE]'
                >
                    <tr>
                        <th
                            className='p-2 h-10 w-60 justify-center'
                        >
                            {`Stock`}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td
                            className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                        >
                            {book?.stock}
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default BookStock
