import { Book } from '@/entities';
import React from 'react'

interface Props {
    book?: Book;
}

function BookPrice({ book }: Props) {
    return (
        <>
            <table
                className='table-fixed text-center justify-center m-5'
            >
                <thead
                    className='bg-[#EEEEEE] border border-[#EEEEEE]'
                >
                    <tr>
                        <th
                            className='p-2 h-10 w-60 justify-center'
                        >
                            {`Prix`}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td
                            className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                        >
                            {book?.price}
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default BookPrice
