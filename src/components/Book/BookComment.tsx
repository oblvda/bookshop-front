import { Book } from '@/entities';
import React from 'react'

interface Props {
    book?: Book;
}

function BookComment({ book }: Props) {
    return (
        <>
            <table
                className='table-fixed text-center justify-center m-5 md:w-full flex-1'
            >
                <thead
                    className='bg-[#EEEEEE] border border-[#EEEEEE]'
                >
                    <tr>
                        <th
                            className='p-2 h-10 w-60 justify-center'
                        >
                            {`Commentaire`}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td
                            className='p-2 h-64 flex border border-[#CECECE] bg-[#EEEEEE]'
                        >
                            {book?.comment}
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default BookComment
