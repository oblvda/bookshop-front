import { Book } from '@/entities';
import React from 'react'

interface Props {
    book?: Book;
}

function BookTitle({ book }: Props) {
    return (
        <>
            <table
                className='table-fixed text-center justify-center m-5'
            >
                <thead
                    className='flex bg-[#EEEEEE] border border-[#EEEEEE]'
                >
                    <tr>
                        <th
                            className='p-2 h-10 w-60 justify-center'
                        >
                            {`Titre du livre`}
                        </th>
                    </tr>
                </thead>
                <tbody
                    className='flex border border-[#EEEEEE] p-2 h-full w-60 justify-center'
                >
                    <tr>
                        <td>
                            {book?.title}
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default BookTitle
