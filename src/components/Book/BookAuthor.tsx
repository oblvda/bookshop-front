import { Book } from '@/entities';
import React from 'react'

interface Props {
    book?: Book;
}

function BookAuthor({ book }: Props) {
    return (
        <>
            <table
                className='table-fixed text-center justify-center m-5'
            >
                <thead
                    className='bg-[#EEEEEE] border border-[#EEEEEE]'
                >
                    <tr>
                        <th
                            className='p-2 h-10 w-60 justify-center'>
                            {`Auteur`}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr
                        className='p-2 h-10 w-60'
                    >
                        <td
                            className='flex border border-[#EEEEEE] p-2 h-full w-60 justify-center'
                        >
                            {book?.author?.name} {book?.author?.lastname}
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default BookAuthor
