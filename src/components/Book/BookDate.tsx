import { Book } from '@/entities';
import dayjs from 'dayjs';
import React from 'react'

interface Props {
    book?: Book;
}

function BookDate({ book }: Props) {
    return (
        <>
            <table
                className='table-fixed text-center justify-center m-5'
            >
                <thead
                    className='bg-[#EEEEEE] border border-[#EEEEEE]'
                >
                    <tr>
                        <th
                            className='p-2 h-10 w-60 justify-center'
                        >
                            {`Date de publication`}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td
                            className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                        >
                            {dayjs(book?.publicationDate).format("YYYY")}
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default BookDate
