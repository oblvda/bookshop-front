import React, { FormEvent, useEffect, useState } from 'react'
import ValidateButton from '../Book/ValidateButton'
import { Author, Book, Genre, Publisher } from '@/entities'
import { fetchAllAuthors } from '@/author-service';
import { useRouter } from 'next/router';
import { fetchAllGenres } from '@/genre-service';
import { fetchAllPublishers } from '@/publisher-service';

interface Props {
    onSubmit: (book: Book) => void;
}

function BookForm({ onSubmit }: Props) {
    const router = useRouter();
    const [authors, setAuthors] = useState<Author[]>([]);
    const [genres, setGenres] = useState<Genre[]>([]);
    const [publishers, setPublishers] = useState<Publisher[]>([]);
    const [errors, setErrors] = useState('');
    const [book, setBook] = useState<Book>({
        title: '',
        publicationDate: '',
        stock: 0,
        author: { id: 0 },
        genre: { id: 0 },
        publisher: { id: 0 },
        isbn: '',
        price: 0,
        comment: '',
    });

    useEffect(() => {
        fetchAllAuthors()
            .then(data => setAuthors(data))
        fetchAllGenres()
            .then(data => setGenres(data))
        fetchAllPublishers()
            .then(data => setPublishers(data))
            .catch(error => {
                if (error.response == 404) {
                    router.push('/404');
                }
            });
    }, []);

    function handleChange(event: any) {
        const value = event.target.type === 'number' ? parseFloat(event.target.value) : event.target.value;
        setBook({
            ...book,
            [event.target.name]: event.target.name === 'idGenre' || event.target.name === 'idPublisher' || event.target.name === 'idAuthor' ? parseInt(value, 10) : value
        });
    }

    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        try {
            onSubmit(book);
        } catch (error: any) {
            if (error.response.status == 400) {
                setErrors("Merci de renseigner des informations valides");
            }
        }
    }

    return (
        <>
            {errors &&
                <p
                    className='text-red-500'
                >
                    {errors}
                </p>}
            <form
                className='justify-center items-center flex-wrap flex flex-row'
                onSubmit={handleSubmit}
            >
                <div
                    className='flex flex-col'
                >
                    <div
                        className='flex flex-row flex-wrap justify-center'
                    >
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='flex bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`Titre du livre`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody
                                className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                            >
                                <tr>
                                    <td>
                                        <input
                                            required
                                            type="text"
                                            name="title"
                                            value={book?.title}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`Date de publication`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td
                                        className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                                    >
                                        <input
                                            required
                                            type="date"
                                            name="publicationDate"
                                            value={String(book?.publicationDate)}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`Stock`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td
                                        className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                                    >
                                        <input
                                            required
                                            type="number"
                                            name="stock"
                                            value={Number(book?.stock)}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`Genre`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td
                                        className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                                    >
                                        <select
                                            required
                                            name="idGenre"
                                            value={Number(book.idGenre)}
                                            onChange={handleChange}
                                        >
                                            <option value={0}>Sélectionner une valeur</option>
                                            {genres.map(genre =>
                                                <option
                                                    value={Number(genre.id)}
                                                    key={genre.id}
                                                >
                                                    {genre.genreLabel}
                                                </option>
                                            )}
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'>
                                        {`Auteur`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr
                                    className='p-2 h-10 w-60'
                                >
                                    <td
                                        className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                                    >
                                        <select
                                            required
                                            name="idAuthor"
                                            value={Number(book.idAuthor)}
                                            onChange={handleChange}
                                        >
                                            <option value={0}>Sélectionner une valeur</option>
                                            {authors.map(author =>
                                                <option
                                                    value={Number(author.id)}
                                                    key={author.id}
                                                >
                                                    {author.name} {author.lastname}
                                                </option>
                                            )}
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`Maison d'édition`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td
                                        className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                                    >
                                        <select
                                            required
                                            name="idPublisher"
                                            value={Number(book.idPublisher)}
                                            onChange={handleChange}
                                        >
                                            <option value={0}>Sélectionner une valeur</option>
                                            {publishers.map(publisher =>
                                                <option
                                                    value={Number(publisher.id)}
                                                    key={publisher.id}
                                                >
                                                    {publisher.publisherLabel}
                                                </option>
                                            )}
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`ISBN`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td
                                        className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                                    >
                                        <input
                                            required
                                            type="text"
                                            name="isbn13"
                                            value={book?.isbn13}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`Prix`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td
                                        className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                                    >
                                        <input
                                            required
                                            type="number"
                                            step="0.01"
                                            name="price"
                                            value={Number(book?.price)}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table
                            className='table-fixed text-center justify-center m-5 md:w-full flex-1'
                        >
                            <thead
                                className='bg-[#EEEEEE] border border-[#EEEEEE]'
                            >
                                <tr>
                                    <th
                                        className='p-2 h-10 w-60 justify-center'
                                    >
                                        {`Commentaire`}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td
                                        className='p-2 h-64 flex border border-[#CECECE] bg-[#EEEEEE]'
                                    >
                                        <textarea
                                            className="bg-[#EEEEEE] w-full h-full"
                                            name="comment"
                                            value={book?.comment}
                                            onChange={handleChange}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div
                        className='justify-end flex flex-row'
                    >
                        <ValidateButton />
                    </div>
                </div>
            </form>
        </>
    )
}

export default BookForm
