import React from 'react'
import PlusButton from '../Dashboard/PlusButton'
import Link from 'next/link'

function AddBookToOrderTable() {
    return (
        <>
            <table className="border-separate border-spacing-0 table-fixed text-center justify-center">
                <thead className='bg-[#EEEEEE]'>
                    <tr>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Titre du livre
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Auteur
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Date de publication
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Maison d’édition
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Stock
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <PlusButton />
                        </td>
                    </tr>
                    <tr>
                        <td
                            className='p-2 h-10 w-60'
                        >
                            <Link
                                href=''
                                className='underline'
                            >
                                The Sliding Mr. Bones (Next Stop, Pottersville)
                            </Link>
                        </td>
                        <td
                            className='p-2 h-10 w-60'
                        >
                            Malcolm Lockyer
                        </td>
                        <td
                            className='p-2 h-10 w-60'
                        >
                            1961
                        </td>
                        <td
                            className='p-2 h-10 w-60'
                        >
                            Penguin Books
                        </td>
                        <td
                            className='p-2 h-10 w-60'
                        >
                            2
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default AddBookToOrderTable
