import React from 'react'

function OrderDate() {
    return (
        <>
            <table
                className='table-fixed text-center justify-center m-5'
            >
                <thead
                    className='bg-[#EEEEEE] border border-[#EEEEEE]'
                >
                    <tr>
                        <th
                            className='p-2 h-10 w-60 justify-center'>
                            {`Date de commande`}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr
                        className='p-2 h-10 w-60'
                    >
                        <td
                            className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                        >
                            {`21/10/2023`}
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default OrderDate
