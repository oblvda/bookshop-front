import React from 'react'

function OrderComment() {
    return (
        <>
            <table
                className='table-fixed text-center justify-center m-5 md:w-full flex-1'
            >
                <thead
                    className='bg-[#EEEEEE] border border-[#EEEEEE]'
                >
                    <tr>
                        <th
                            className='p-2 h-10 w-60 justify-center'
                        >
                            {`Commentaire`}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td
                            className='p-2 h-64 flex border bg-[#EEEEEE] border-[#CECECE]'
                        >
                            {`Lorem ipsum`}
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default OrderComment
