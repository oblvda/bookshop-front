import React from 'react'
import PlusButton from '../Dashboard/PlusButton'
import Link from 'next/link'
import { Order } from '@/entities';
import dayjs from 'dayjs';

interface Props {
    orders?: Order[];
}

function OrderTable({ orders }: Props) {
    return (
        <>
            <table className="border-separate border-spacing-0 table-fixed text-center justify-center">
                <thead className='bg-[#EEEEEE]'>
                    <tr>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Numéro de commande
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Livre commandé
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Libraire
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Nombre de livres
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Date de commande
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Date de réception
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <PlusButton />
                        </td>
                    </tr>
                    {orders?.map(order =>
                        <tr key={order.id}>
                            <td
                                className='p-2 h-10 w-60'
                            >
                                <Link
                                    href=''
                                    className='underline'
                                >
                                    #{order.id}
                                </Link>
                            </td>
                            <td
                                className='p-2 h-10 w-60'
                            >
                                {order.book?.title}
                            </td>
                            <td
                                className='p-2 h-10 w-60'
                            >
                                {order.user?.firstname} {order.user?.lastname}
                            </td>
                            <td
                                className='p-2 h-10 w-60'
                            >
                                {order.bookQuantity}
                            </td>
                            <td
                                className='p-2 h-10 w-60'
                            >
                                {dayjs(order.receptionDate).format("DD/MM/YYYY")}
                            </td>
                            <td
                                className='p-2 h-10 w-60'
                            >
                                {order.total}
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        </>
    )
}

export default OrderTable
