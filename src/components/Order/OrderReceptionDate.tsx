import React from 'react'

function OrderReceptionDate() {
    return (
        <>
            <table
                className='table-fixed text-center justify-center m-5'
            >
                <thead
                    className='bg-[#EEEEEE] border border-[#EEEEEE]'
                >
                    <tr>
                        <th
                            className='p-2 h-10 w-60 justify-center'>
                            {`Date de réception`}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr
                        className='p-2 h-10 w-60'
                    >
                        <td
                            className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                        >
                            {`06/02/2024`}
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default OrderReceptionDate
