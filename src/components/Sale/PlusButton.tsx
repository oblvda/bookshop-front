import Link from 'next/link'
import React from 'react'

function PlusButton() {
    return (
        <div>
            <button
                className='grid grid-flow-col justify-stretch items-center h-10 w-full bg-[#3100A2] text-white hover:text-[#3100A2] hover:bg-white'
            >
                <Link
                    href={"/sale/add"}>
                    {`+`}
                </Link>
            </button>
        </div>
    )
}

export default PlusButton
