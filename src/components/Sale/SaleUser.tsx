import { Sale } from '@/entities'
import React from 'react'

interface Props {
    sale?: Sale
}

function SaleUser({ sale }: Props) {
    return (
        <>
            <table
                className='table-fixed text-center justify-center m-5'
            >
                <thead
                    className='flex bg-[#EEEEEE] border border-[#EEEEEE]'
                >
                    <tr>
                        <th
                            className='p-2 h-10 w-60 justify-center'
                        >
                            {`Vendeur`}
                        </th>
                    </tr>
                </thead>
                <tbody
                    className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                >
                    <tr>
                        <td>
                            {sale?.user?.firstname} {sale?.user?.lastname}
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default SaleUser
