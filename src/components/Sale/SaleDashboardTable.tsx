import { Sale } from '@/entities'
import dayjs from 'dayjs';
import Link from 'next/link';
import React from 'react'
import PlusButton from './PlusButton';

interface Props {
    sales?: Sale[];
}

function SaleDashboardTable({ sales }: Props) {
    return (
        <>
            <table className="border-separate border-spacing-0 table-fixed text-center justify-center">
                <thead className='bg-[#EEEEEE]'>
                    <tr>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Date de la vente
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Livre vendu
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Quantité
                        </th>
                        <th
                            className='p-2 h-10 w-60'
                        >
                            Vendeur
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <PlusButton />
                        </td>
                    </tr>
                    {sales?.map(sale =>
                        <tr key={sale.id}>
                            <td
                                className='p-2 h-10 w-60'
                            >
                                <Link
                                    href={'/sale/' + sale.id}
                                    className='underline'
                                >
                                    {dayjs(sale.saleDate).format("DD/MM/YYYY")}
                                </Link>

                            </td>
                            <td
                                className='p-2 h-10 w-60'
                            >
                                {sale.book?.title}
                            </td>
                            <td
                                className='p-2 h-10 w-60'
                            >
                                {sale.bookQuantity}
                            </td>
                            <td
                                className='p-2 h-10 w-60'
                            >
                                {sale.user?.firstname} {sale.user?.lastname}
                            </td>
                        </tr>
                    )}
                </tbody>
            </table >
        </>
    )
}

export default SaleDashboardTable
