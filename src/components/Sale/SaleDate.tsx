import { Sale } from '@/entities'
import dayjs from 'dayjs';
import React from 'react'

interface Props {
    sale?: Sale;
}

function SaleDate({ sale }: Props) {
    return (
        <>
            <table
                className='table-fixed text-center justify-center m-5'
            >
                <thead
                    className='flex bg-[#EEEEEE] border border-[#EEEEEE]'
                >
                    <tr>
                        <th
                            className='p-2 h-10 w-60 justify-center'
                        >
                            {`Date de la vente`}
                        </th>
                    </tr>
                </thead>
                <tbody
                    className='flex border border-[#EEEEEE] p-2 h-10 w-60 justify-center'
                >
                    <tr>
                        <td>
                            {dayjs(sale?.saleDate).format("DD/MM/YYYY")}
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default SaleDate
