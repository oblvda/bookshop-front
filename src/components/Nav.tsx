import Link from 'next/link'
import React from 'react'
import LogoutButton from './Nav/LogoutButton'
import { useRouter } from 'next/router';

function Nav() {
    const { pathname } = useRouter();
    if (pathname !== "/login") {
        return (
            <>
                <nav
                    className='md:flex fixed hidden flex-col h-screen w-1/4 bg-[#3100A2] items-end'
                >
                    <div
                        className='mt-20 flex flex-col'
                    >
                        <ul>
                            <li
                                className='flex items-center h-20 w-52 justify-center border-solid border border-white hover:text-[#3100A2] hover:bg-white'
                            >
                                <Link
                                    href={'/'}
                                    className=''
                                >
                                    <span>
                                        {`Inventaire`}
                                    </span>
                                </Link>
                            </li>
                            <li
                                className='flex items-center h-20 w-52 justify-center border-solid border border-white hover:text-[#3100A2] hover:bg-white'
                            >
                                <Link
                                    href={'/sale/dashboard'}
                                    className=''
                                >
                                    <span>
                                        {`Ventes`}
                                    </span>
                                </Link>
                            </li>
                            <li
                                className='flex items-center h-20 w-52 justify-center border-solid border border-white hover:text-[#3100A2] hover:bg-white'
                            >
                                <Link
                                    href={'/order/dashboard'}
                                    className=''
                                >
                                    <span>
                                        {`Commandes`}
                                    </span>
                                </Link>
                            </li>
                            <li
                                className='flex items-center h-20 w-52 justify-center border-solid border border-white hover:text-[#3100A2] hover:bg-white'
                            >
                                <Link
                                    href={'/'}
                                    className=''
                                >
                                    <span>
                                        {`Profil`}
                                    </span>
                                </Link>
                            </li>
                        </ul>
                        <div
                            className='flex justify-center'
                        >
                            <img
                                className='flex w-20 h-20 m-5'
                                src='/assets/open-book.png'
                            >
                            </img>
                        </div>
                    </div>
                    <div
                        className='flex flex-col mb-20 items-end'
                    >
                        <LogoutButton />
                    </div>
                </nav>
            </>
        )
    } else
        return (undefined)
}

export default Nav
