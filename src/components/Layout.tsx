import React from 'react'
import Nav from './Nav'
import HamburgerNav from './HamburgerNav'

function Layout({ children }: any) {
  return (
    <>
      <Nav />
      <HamburgerNav />
      {children}
    </>
  )
}

export default Layout
