import { AuthContext } from '@/auth/auth-context';
import { useRouter } from 'next/router';
import React, { useContext } from 'react'

function LogoutButton() {
    const { setToken } = useContext(AuthContext);
    const router = useRouter();

    function logout() {
        setToken(null);
        router.replace('/login');
    }

    return (
        <>
            <button
                className='flex grid grid-flow-col justify-stretch items-center h-20 w-52 bg-[#3100A2] text-white border-solid border border-white hover:text-[#3100A2] hover:bg-white'
                onClick={logout}
            >
                {`Se déconnecter`}
            </button>
        </>
    )
}

export default LogoutButton
