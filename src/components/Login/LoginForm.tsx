import { AuthContext } from '@/auth/auth-context';
import { postLogin } from '@/auth/auth-service';
import { User } from '@/entities';
import { useRouter } from 'next/router';
import React, { useContext, useState } from 'react';

function LoginForm() {
    const router = useRouter();
    const { setToken } = useContext(AuthContext);
    const [user, setUser] = useState<User>({
        email: '',
        password: ''
    });
    const [error, setError] = useState('');

    function handleChange(event: any) {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event: any) {
        setError('');
        event.preventDefault();
        try {
            const data = await postLogin(user.email, user.password);
            setToken(data.token);
            router.replace('/');
        } catch (error: any) {
            if (error.response?.status == 401) {
                setError('Mot de passe ou identifiant invalide');
            } else if (error.response?.status == 500) {
                setError('Erreur serveur');
            }
        }
    }

    return (
        <>
            {error &&
                <p
                    className='text-red-500'
                >
                    {error}
                </p>}
            <form
                className='h-full flex flex-col items-center justify-center text-center'
                onSubmit={handleSubmit}
            >
                <input
                    name='email'
                    className="appearance-none border w-64 h-16 p-4 m-4 text-[#CECECE] bg-[#EEEEEE] leading-tight focus:outline-none focus:shadow-outline"
                    type='email'
                    placeholder="Email"
                    onChange={handleChange}
                />
                <input
                    name='password'
                    className="appearance-none border w-64 h-16 p-4 m-4 text-[#CECECE] bg-[#EEEEEE] leading-tight focus:outline-none focus:shadow-outline"
                    type='password'
                    placeholder="Mot de passe"
                    onChange={handleChange}
                />
                <button
                    type='submit'
                    className='h-16 bg-[#3100A2] m-4 text-white w-64'
                >
                    {`Se connecter`}
                </button>
            </form>
        </>
    )
}

export default LoginForm